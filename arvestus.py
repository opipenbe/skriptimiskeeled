#!/usr/bin/python
# -*- coding: utf-8 -*-
# Olari Pipenberg A21
import sys
import urllib
import re
from time import gmtime, strftime
# Exit codes:
# 1 - wrong argument number

def parse_file_lines(input_file):
    lines = []
    try:
        with open(input_file) as f:
            for line in f:
                lines.append(line)
    except IOError:
            print "Error with input file"
            sys.exit(1)
    return lines


def find_string_from_web(website, search_pattern):
    try:
        website = urllib.urlopen(website).read()
    except:
        website = ''
    urls = re.findall(search_pattern,website)
    if not urls:
        return False
    else:
        return True


if len(sys.argv) != 3:  # wrong arg numbers
    print "Usage: " + sys.argv[0] + " itcollege.ee n2idis.txt"
    sys.exit(1)

server = sys.argv[1]
file_arg = sys.argv[2]

parsed_lines = parse_file_lines(file_arg)


for line in parsed_lines:

    url = "http://www."+server+line.split(',', 1)[0]
    pattern = line.split(',', 1)[-1]
    pattern = pattern.strip() # erase whitespace
    state = find_string_from_web(url, pattern)
    if state == True:
        state = "OK"
    else:
        state = "NOK"

    print url+","+pattern+","+strftime("%Y-%m-%d_%H-%M-%S", gmtime())+","+state


