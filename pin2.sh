#!/bin/bash
#Autor: Olari Pipenberg A21, 2016
#Testitud Ubuntu 14.04'ga
#Skriptimiskeeled Kontrolltöö
export LC_ALL=C


#TODO argument check


FLAG=0
NUMBER=$1
PIN=0
FILE_PATH=""

while [ $PIN -lt 10000 ]; do


    echo $PRINT_PIN
	#Kirjutame faili
	if [ ${FLAG} -eq 1 ]; then
		echo $PRINT_PIN >> $FILE_PATH
	fi

	#0-de lisamine ette	
	PRINT_PIN=$PIN
	if [ ${#PRINT_PIN} \< 4 ]; then
		DIFF=$((4 - ${#PRINT_PIN}))
		for ((i=0; i<DIFF; i++)); do
			PRINT_PIN="0$PRINT_PIN"
		done
	fi
	

	if [ "$PRINT_PIN" -eq "$NUMBER" ]; then
		echo "PIN leitud, katkestame ajutiselt otsimise!"		
		test -d /$PRINT_PIN || mkdir -p $PRINT_PIN
		test -a $PRINT_PIN/koodid.txt
		if [ $? -ne "0" ]; then
			FILE_PATH=$PRINT_PIN/koodid.txt
		else
			echo "$PRINT_PIN/koodid.txt eksisteerib"
			STAMP=$(date +%s)
			FILE_PATH=$PRINT_PIN/koodid.txt.$STAMP
		fi
	
		FLAG=1
	fi
	
	PIN=$(($PIN + 1))

done
