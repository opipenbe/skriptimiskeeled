#!/usr/bin/python
# -*- coding: utf-8 -*-
# Olari Pipenberg A21

import sys
import random
from string import digits, uppercase, lowercase

# Exit codes:
# 1 - wrong argument number
# 2 - output and input files are same
# 3 - error with input file
# 4 - error with output file
# 5 - line index error


def parse_lines(input_file):  # creates list,saves only necessary infromation
    lines = []
    try:
        with open(input_file) as f:  # convert input file to list
            next(f)  # ignore first line
            for line in f:
                if line.strip():  # ignore blank lines
                    line = line.split('\t', 1)[-1]  # remove entry number, tab
                    lines.append(line)
    except IOError:
            print "Error with input file"
            sys.exit(3)
    return lines


def get_data(data_lines):  # creates lists from names and notes
    first_name=[]
    last_name=[]
    note=[]
    for line in data_lines:
        try:
            first_name.append(line.split(' ')[0].strip())
            last_name.append(line.split(' ')[1].strip())
            note.append(line.split(' ')[2].strip())
        except IndexError:
            print "Line reading error, sorry!"
            sys.exit(5)
    return first_name, last_name, note


def create_user(first_name, last_name):  # creates i-tee username from first and lastname
    username = (first_name[:1] + last_name[:7]).lower()
    return username


def generate_token(start_string):  # generates token with note string
    token_hash = ''.join(random.SystemRandom().choice(digits + uppercase + lowercase + '_' + '-') for i in range(20))
    start_string = start_string + token_hash[len(start_string):]
    return start_string


if __name__ == "__main__":

    domain = "@itcollege.ee"  # email domain

    if len(sys.argv) != 3:  # wrong arg numbers
        print "Usage: " + sys.argv[0] + " <input file> <output file>"
        sys.exit(1)

    if sys.argv[1] == sys.argv[2]:
        print "Sorry! Output and input files cannot be same"
        sys.exit(2)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    parsed_lines = parse_lines(input_file)  # save only necessary infromation from input file
    first_name, last_name, note = get_data(parsed_lines)  # create lists from names and notes

    try:
        output_file = open(output_file, 'w')
    except IOError:
        print "Error with output file"
        sys.exit(4)

    line_index = 0
    for line in parsed_lines:  # finally write text to file
        output_file_line = create_user(first_name[line_index], last_name[line_index])+","  # add username
        output_file_line = output_file_line+first_name[line_index]+" "+last_name[line_index]+","  # first name, last name
        output_file_line = output_file_line+first_name[line_index].lower()+"."+last_name[line_index].lower()+domain+","  # email
        output_file_line = output_file_line+generate_token(note[line_index])+"\n"
        output_file.write(output_file_line)
        line_index += 1

