#/bin/bash
#Autor: Olari Pipenberg, 2016
#Testitud Ubuntu 14.04'ga
#Skriptimiskeeled 1. kodutöö (bash)

#V2ljumiskoodid 
#1 - k6ik veakoodid on selle numbriga

#Argumentide kontroll
if [ $# -eq 2 ]; then
    DIR=$1
    GROUP=$2
    SHARE=$(basename $DIR)
    else
        if [ $# -eq 3 ]; then
            DIR=$1
            GROUP=$2
            SHARE=$3
         else
            echo "Kasutamine: $(basename $0) KAUST GRUPP <JAGATUD KAUST>"
            exit 1
         fi
fi

#Privileegide kontroll
if [ $"$EUID" -ne 0 ]; then
    echo "Puuduvad vastavad 6igused!"
    exit 1
fi

#TODO Check if arguments are appropriate

#Kontrollime kas samba on olemas. jah tean, seda saab ka lyhemalt
INPUT=$(dpkg -s samba | grep "install ok" | head -1)
if [[ $INPUT != *"installed"* ]]; then
   echo "Samba on paigaldamata, proovime paigaldada.";
   sleep 5; #Vajadusel eemaldada
   sudo apt-get update &&
   sudo apt-get install samba -y
   INPUT=$(dpkg -s samba | grep "install ok" | head -1)
   if [[ $INPUT != *"installed"* ]]; then
        echo "Ei suutnud paigaldada sambat :(";
        exit 1;
   fi
fi

#Kontrolli v6i loo kataloom
test -d $DIR || 
                {
                echo "Puudub pakutud kataloom, loome selle..."
                sleep 5;
                mkdir -p $DIR
                }

#Kontrolli v6i loo grupp
getent group $GROUP > /dev/null || 
                                    {
                                    echo "Puudub pakutud grupp, loome uue grupi...";
                                    sleep 5; #Vajadusel eemaldada
                                    addgroup $GROUP
                                    }

#Loome backupi originaalfailist (otseselt pole vaja)
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

#Loome test faili
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.test

#Uue confi lisamine
cat >> /etc/samba/smb.conf.test << LOPP

echo[$SHARE]
    comment=jagatud kataloom
    path=$DIR
    writable=yes
    valid users=@$GROUP
    force group=$GROUP
    browsable=yes
    create mask=0664
    directory mask=0775
LOPP

#Kontrollime confi
testparm -s /etc/samba/smb.conf.test
if [ $? -ne 0 ]; then
    echo "Confis on viga, ei saa kahjuks rakendada uusi seadeid!"
    exit 1
fi
    
#Viime muudatused live
sudo cp /etc/samba/smb.conf.test /etc/samba/smb.conf
sudo /etc/init.d/smbd reload
echo "Great success!"

#smbclient //localhost/<nimi>
