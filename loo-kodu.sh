#!/bin/bash
#Autor: Olari Pipenberg A21, 2016
#Testitud Ubuntu 14.04'ga
#Skriptimiskeeled 2. kodutöö (bash)
#Veakoodid:
#	exit 1 - argumendi viga
#	exit 2 - puuduvad 6igused
# 	exit 3 - apache paigaldus ei 6nnestunud
export LC_ALL=C

#Argumendi kontroll
if [ $# -ne 1 ]; then
    echo "Kasutamine: $(basename $0) www.veebileht.ee"
    exit 1
fi

#Privileegide kontroll
if [ $"$EUID" -ne 0 ]; then
    echo "Puuduvad vastavad 6igused!"
    exit 2
fi

#Kontrollime, kas apache2 on olemas.
INPUT=$(dpkg -s apache2 2> /dev/null | grep "install ok" | head -1)
if [[ $INPUT != *"installed"* ]]; then
   echo "Paigaldame vajalikud tarkvarapaketid."
   sleep 5; #Vajadusel eemaldada, rohkem demomise jaoks
   apt-get update &&
   apt-get install apache2 -y
   #Kontrollime yhe korra veel
   INPUT=$(dpkg -s apache2 2> /dev/null | grep "install ok" | head -1)
   if [[ $INPUT != *"installed"* ]]; then
        echo "Ei suutnud paigaldada apachet :("
        exit 3;
   fi
fi

#Kontrollime eksisteerivaid ridu ja vajadusel loome nimelahenduse
FILE_PATH="/etc/hosts"
IP="127.0.0.1"
INPUT=$(cat $FILE_PATH | grep " $1")
if [[ $INPUT == *"$1" ]]; then
	echo \"$1\"" juba eksisteerib $FILE_PATH failis, ei hakka uut lisama"
else
	echo $IP $1 >> $FILE_PATH
fi

#Loome veebilehe katalooma
FILE_PATH="/var/www/$1"
test -d $FILE_PATH
if [ $? -ne "0" ]; then
	mkdir -p $FILE_PATH
else
	echo "$FILE_PATH eksisteerib, ei hakka uut looma"
fi

#Loome veebilehe (debugimise m6ttes j22b sisuks lehe nimi)
FILE_PATH="/var/www/$1/index.html"
test -a $FILE_PATH
if [ $? -ne "0"  ]; then
	echo $1 > $FILE_PATH
else
	echo "$FILE_PATH eksisteerib, ei hakka uut looma"
fi

#APACHE seadistamine
FILE_PATH="/etc/apache2/sites-available/$1.conf"
test -a $FILE_PATH
if [ $? -ne "0"  ]; then
	cp /etc/apache2/sites-available/000-default.conf $FILE_PATH #default confist uus conf 
	sed -i "s@#ServerName www.example.com@ServerName $1@" $FILE_PATH
	sed -i "s@DocumentRoot /var/www/html@DocumentRoot /var/www/$1@" $FILE_PATH
	#Lubame lehe konfi t22le panna
	a2ensite $1.conf > /dev/null
	#L6puks taask2ivitame lehe
	service apache2 reload > /dev/null
	if [ $? -ne "0" ]; then
		echo "apache2 taask2ivitusel tekkis viga"
	fi
	echo "Great success!"
else
	echo "$FILE_PATH eksisteerib, ei hakka uut looma"
fi
